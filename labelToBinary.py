# -*- coding: utf-8 -*-
from PIL import Image
import numpy as np


for i in range(1,21):
    
    path = "data/test/mask/testB_" + str(i) + "_anno.bmp"
    
    #Read Image 
    img = Image.open(path)  
    
    # Convert Image to Numpy as array 
    img = np.array(img)  
    
    # Put threshold to make it binary
    binarr = np.where(img>0, 255, 0)
    binarr = binarr.astype(np.uint8)
    
    # Covert numpy array back to image 
    binimg2 = Image.fromarray(binarr.astype(np.uint8))
    
    # Save image
    binimg2.save(path)
    
    
    
    






