# Project 9: Segmentation of Cell Images

##### Course: 
* 02456, Deep Learning Fall 2020


##### Students: 
* Niels With Mikkelsen, s174290
* Victor Foged, s174267
* Lasse Pedersen, s174253

##### Supervisor:
* Peter Jensen, Cellari.io

#### Project Description
**Segmentation of cell images**. [Cellari.io](https://cellari.io/) provides a generic platform that allows non-experts to use deep learning to solve pixel-wise segmentation tasks. We will focus on using U-Net and Mask-RCNN for pixel-wise segmentation and separation of cells. Clinical images from hospital use-cases will be provided by Cellari. Supervised by Peter Jensen, peter.jensen@cellari.io. [Video presentation here.](https://drive.google.com/file/d/1iiK80QjrZ8gsAs2-vaEJ7o05eJYisVba/view?usp=sharing)


#### Files Included
* In order to run our implementation and recreate the results of our U-net model, please run the file **unet.ipynb**.
* To run our Mask R-CNN implementation, use the file **MaskRCNN.ipynb**.

##### Folder Structure


* The folder **Mask Identifications** contains images with predictions from our test set for selected images.
* The **results**-folder contains a summary of evaluations metrics different versions of our models. 
* The **data** folder contains our training and test set. 
* **LoadModel.ipynb** is used to calculate our evaluation metrics. 

The remaining python files in the root folder are used to run the MaskRCNN model. 