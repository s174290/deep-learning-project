100 epochs

Test A
------------------------------------------
Count mean: 0.5241800269130635
F1 mean: 0.7718312532455786
F1 Object mean: 0.6807338997437167
Hausdorff Object distance mean: 134.93336791021062

Test B
------------------------------------------
Count mean: 0.4766284584009043
F1 mean: 0.7915712179100406
F1 Object mean: 0.7398366785542502
Hausdorff Object distance mean: 86.11905817403746
ª
