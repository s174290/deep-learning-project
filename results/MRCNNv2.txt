------------------Test Set A---------------------
Count mean: 0.8449257424333053
F1 mean: 0.9171235652626236
F1 Object mean: 0.9050064458354974
Hausdorff Object distance mean: 42.92767124710971
------------------Test Set B---------------------
Count mean: 0.7821536796536795
F1 mean: 0.8984655478680692
F1 Object mean: 0.8668620603227085
Hausdorff Object distance mean: 85.10839364800891